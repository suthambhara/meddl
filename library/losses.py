import torch
import torch.nn as nn
from pytorch_msssim import ssim, ms_ssim, SSIM, MS_SSIM


class MedDLLoss(nn.Module):
    def __init__(self, name: str, loss_params: dict):
        super(MedDLLoss, self).__init__()
        self.name = name
        if name == 'MSE':
            self.loss_fn = torch.nn.MSELoss()
        elif name == 'L1':
            self.loss_fn = torch.nn.L1Loss()
        elif name == 'MSSIM':
            assert loss_params is not None, "MSSIM requires parameters"
            data_range = loss_params['data_range']
            nr_channels = loss_params['nr_channels']
            self.loss_fn = MS_SSIM(
                data_range=data_range, size_average=True, channel=nr_channels)
        else:
            raise NotImplementedError("Uknown loss function")

    def forward(self, data_dictionary: dict):
        target = data_dictionary['target']
        data = data_dictionary['result']['output']
        if self.name == 'MSSIM':
            return 1.0 - self.loss_fn(data, target)
        return self.loss_fn(data, target)


class L1_MSSSIM(nn.Module):
    def __init__(self, loss_params: dict):
        super(L1_MSSSIM, self).__init__()
        data_range = loss_params['data_range']
        nr_channels = loss_params['nr_channels']
        self.mssim_loss_fn = MS_SSIM(
            data_range=data_range, size_average=True, channel=nr_channels)
        self.l1_loss_fn = torch.nn.L1Loss()

    def forward(self, data_dictionary: dict):
        target = data_dictionary['target']
        data = data_dictionary['result']['output']
        mssim_l = 1.0 - self.mssim_loss_fn(data, target)
        l1_loss = self.l1_loss_fn(data, target)
        l1_wt = 0.16
        total_loss = mssim_l * (1.0-l1_wt) + l1_loss * l1_wt
        return total_loss


class BCELossWithLogits(nn.Module):
    def __init__(self, loss_params: dict):
        super(BCELossWithLogits, self).__init__()

    def forward(self, data_dictionary: dict):
        target = data_dictionary['target']
        data = data_dictionary['result']['output']
        pos = torch.sum(target)
        neg = torch.numel(target) - pos
        pos_weight = None
        if neg > 1:
            pos_weight = neg / pos

        loss = nn.functional.binary_cross_entropy_with_logits(
            data, target, pos_weight=pos_weight)
        loss = torch.mean(loss)
        return loss
