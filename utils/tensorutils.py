import torch


def move_tensors_to_device(inputs: dict, device: str):
    """
        Moves all tensors in the dictionary to device
        Arguments:
            inputs[dict]
                Dictionary of tensors

            device[str]
                Device name

    """
    for elem in inputs:
        if type(inputs[elem]) is torch.Tensor:
            inputs[elem] = inputs[elem].to(device)
