import torch
import numpy
from Denoise.data_transform import DataTransforms
from Denoise.dataset import NaturalImagesTif, NaturalImagesJpg
from Denoise.denoise_model import DenoiseModel
from library.model import Model
import pathlib
import argparse

default_chkpt = None
default_chkpt = pathlib.Path(
    "Denoise/models/latest_Denoise_Gaussian_rgb_1.pt")


def create_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=pathlib.Path, default=pathlib.Path("/mnt/aidata/data/Denoise/NaturalImages/rgb/train"),
                        help='Path to the tiff data')
    parser.add_argument('--validation-data', type=pathlib.Path, default=pathlib.Path("/mnt/aidata/data/Denoise/NaturalImages/rgb/test"),
                        help='Path to the tiff data')
    parser.add_argument('--epochs', default=50000, type=int,
                        help='Number of epochs of training')
    parser.add_argument('--batch-size', type=int, default=4,
                        help='Size of the batches used during training')
    parser.add_argument('--nr_channels', type=int, default=3,
                        help='Size of the batches used during training')
    parser.add_argument('--id', type=str, default="Denoise_Gaussian_rgb_1",
                        help='id of the run')
    parser.add_argument('--checkpoint', type=pathlib.Path, default=default_chkpt,
                        help='Model checkpoint to restart the training')
    parser.add_argument('--export-folder', type=pathlib.Path, default=pathlib.Path('Denoise/models'),
                        help='model export folder')
    parser.add_argument('--logdir', type=pathlib.Path, default=pathlib.Path('Denoise/logs'),
                        help='Logging directory for Tensorboard')
    return parser


def get_lr(epoch: int) -> float:
    n = 0.1
    return n


if __name__ == '__main__':
    args = create_arg_parser().parse_args()
    normalization = 1 / 10.0

    data = NaturalImagesJpg(
        str(args.data), transform=DataTransforms("training", normalization, noise_range=[0.05, 0.2]), patch_len=200)

    val_data = NaturalImagesJpg(
        str(args.validation_data), transform=DataTransforms("validation", normalization, noise_range=[0.1, 0.1]))

    loader = torch.utils.data.DataLoader(
        data, batch_size=args.batch_size, shuffle=True, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        val_data, batch_size=1, shuffle=False, pin_memory=True)

    if args.checkpoint != None:
        model = Model.load_model(
            DenoiseModel, str(args.checkpoint), training=True, load_optimizer=True)
        training_config = {'epochs': args.epochs, 'id': args.id,
                           'export_folder': str(args.export_folder),
                           'clip_gradient_value': 10.0}
        model.train_model(loader, val_loader, training_config=training_config)
    else:
        model = DenoiseModel(
            {'device': 'cuda', 'in_channels': 3})

        # model.compile_model('Adam', {'type': 'L1_MSSSIM', 'params': {
        #                    'nr_channels': 1, 'data_range': 1.0}})

        model.compile_model('Adam', 'L1')

        training_config = {'epochs': args.epochs, 'id': args.id,
                           'export_folder': str(args.export_folder),
                           'clip_gradient_value': 10.0}

        logging_config = {'path': str(args.logdir),
                          'batch_interval': 0, 'epoch_interval': 1}

        scheduling_config = {'type': 'LambdaLR', 'params': {'lambda': get_lr}}

        model.train_model(loader, val_loader, training_config, scheduler_config=scheduling_config, validation_config={'epoch_interval': 4, 'batch_interval': -1},
                          logging_config=logging_config)
