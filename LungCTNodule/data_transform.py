import torch
import numpy as np
from torchvision import transforms
from typing import Tuple
from scipy import ndimage


class DataTransforms:
    def __init__(self, phase: str,):
        self.phase = phase
        if phase == 'training':
            self.transform = transforms.Compose(
                [self.rotate, self.normalize])
        else:
            self.transform = transforms.Compose(
                [self.normalize])

    def rotate(self, input_dict: dict) -> dict:
        data = input_dict['data']
        target = input_dict['target']

        data = np.squeeze(data, 0)
        target = np.squeeze(target, 0)

        in_transformation = np.random.randint(0, 4)
        # augment
        if in_transformation > 0:
            data = np.rot90(data, in_transformation, axes=(-2, -1)).copy()
            target = np.rot90(target, in_transformation, axes=(-2, -1)).copy()

        th_transformation = np.random.randint(0, 4)
        # augment
        if th_transformation > 0:
            data = np.rot90(data, th_transformation, axes=(-3, -1)).copy()
            target = np.rot90(target, th_transformation, axes=(-3, -1)).copy()
        #zoom_factor = np.random.uniform(1.0, 1.5)
        # if zoom_factor > 1.0:
        #     orig_shape = data.shape
        #     data = ndimage.zoom(data, zoom_factor)
        #     target = ndimage.zoom(target, zoom_factor)
        #     new_shape = data.shape
        #     x_start = np.random.randint(0, new_shape[0] - orig_shape[0]+1)
        #     y_start = np.random.randint(0, new_shape[1] - orig_shape[1]+1)
        #     z_start = np.random.randint(0, new_shape[2] - orig_shape[2]+1)

        #     data = data[x_start:x_start+orig_shape[0],
        #                 y_start:y_start+orig_shape[1], z_start:z_start+orig_shape[2]]
        #     target = target[x_start:x_start+orig_shape[0],
        #                     y_start:y_start+orig_shape[1], z_start:z_start+orig_shape[2]]

        #     target[target < 0.95] = 0.0
        #     # rest round to 1.0
        #     target[target > 0.94999] = 1.0

        data = np.expand_dims(data, 0)
        target = np.expand_dims(target, 0)

        input_dict['data'] = data
        input_dict['target'] = target
        return input_dict

    def normalize(self, input_dict):
        data = input_dict['data']

        # CT specific
        lower = -1000.0
        upper = 400.0

        data = (data - lower) / (upper - lower)

        input_dict['data'] = data

        return input_dict

    def __call__(self, input_dict: dict):
        transformed_dict = self.transform(input_dict)
        return transformed_dict
