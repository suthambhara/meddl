from library.model import Model
from library.networks.unet import Unet2D
import torch
import torch.nn as nn
from typing import Tuple


class DenoiseTraceModel(nn.Module):
    def __init__(self, model: Model):
        super().__init__()
        self.model = model

    def forward(self, in_tensor: torch.Tensor) -> torch.Tensor:
        out_tensor_dict = self.model.forward({'data': in_tensor})
        return out_tensor_dict['output']


class DenoiseModel(Model):

    def __init__(self, model_config: dict):
        super(DenoiseModel, self).__init__(model_config)
        self.num_channels_in = model_config["in_channels"]
        self.denoiser = Unet2D(self.num_channels_in, self.num_channels_in)
        self.pad_size = 8
        self.zero_pad = torch.nn.ZeroPad2d(self.pad_size)

    def forward(self, input_dict: dict):
        padded_data = self.zero_pad(input_dict['data'])
        denoised_data = self.denoiser(padded_data)
        denoised_data = denoised_data[..., self.pad_size:-
                                      self.pad_size, self.pad_size:-self.pad_size]
        return {'output': denoised_data}

    def post_model_execution(self, input_dict: dict) -> dict:
        """
        Overriden from Model
        """
        normalization = input_dict['normalization']
        normalization = torch.unsqueeze(normalization, 1)
        normalization = torch.unsqueeze(normalization, 1)
        normalization = torch.unsqueeze(normalization, 1)
        input_dict['result']['output'] = input_dict['result']['output'] / normalization
        input_dict['target'] = input_dict['target'] / normalization
        input_dict['data'] = input_dict['data'] / normalization
        return input_dict

    def get_tracer(self, trace_params: None) -> nn.Module:
        """Overridden routine to create tracer object
        """
        tracer = DenoiseTraceModel(self)
        return tracer
