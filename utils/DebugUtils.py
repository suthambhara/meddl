import numpy as np
import matplotlib.pyplot as plt
from collections.abc import Iterable
import tifffile
import PIL

def WriteTiff(otMagS:np.array, outname):
    """
        Writes a tiff file. Each element is scaled to (0, 4096) and saved

    """
    otMagS = np.squeeze(otMagS)
    minv = np.min(otMagS)
    maxv = np.max(otMagS)
    flm = 4095.0
    otMagS = (otMagS - minv) / (maxv - minv)
    otMagS *= flm
    outname = outname + '.tif'

    tifffile.imsave(outname, otMagS.astype('float32'))

def WriteRec(otMagS, outname):
    """
        Writes a rec file. Each element is scaled to 12 bits and saved

        Arguments:
            otMagS: Magnitude array

            outname: Name of file without the .rec extension
        returns:
            None
    """
    minv = np.min(otMagS)
    maxv = np.max(otMagS)
    flm = 4095.0
    otMagS = (otMagS - minv) / (maxv - minv)
    otMagS *= flm
    shOut = otMagS.astype('uint16')
    rB = shOut.tobytes()
    # os.makedirs(str(args.outdir), exist_ok=True)
    newFile = open(outname+".rec", "wb")
    newFile.write(rB)
    sinStr = "000 0 00: nrs_bytes_per_pixel	       : 2\n 001 0 00: output_resolutions	       :" + \
        str(shOut.shape[-1]) + " " + str(shOut.shape[-2]) + " "+"1\n"
    newSinF = open(outname+".sin", "w")
    newSinF.writelines(sinStr)


def WriteImage(in_arr: np.array, outname):
    """
        Writes a numpy float array to jpg

        Arguments:
            in_arr: float array of the form 3,H,W or 1,3,H,W
            outname : Name of output file without the extension 
    """
    if (len(in_arr.shape) == 4) and (in_arr.shape[0] == 1):
        in_arr = np.squeeze(in_arr, 0)
    in_arr = np.transpose(in_arr, (1, 2, 0))
    minv = np.min(in_arr)
    maxv = np.max(in_arr)
    if (maxv - minv) > 0.0001:
        scaled_arr = (in_arr - minv) / (maxv - minv)
    else:
        scaled_arr = in_arr
    scaled_arr = scaled_arr * 255.0
    scaled_arr = scaled_arr.astype(np.uint8)
    im = PIL.Image.fromarray(scaled_arr)
    im.save(outname+'.jpg')


def plot_value(arr_list: list, names: list = None, share=[False, False]):
    """
    Plots graphs and images in 1D or 2D (images)

    Arguments:
        arr_list: 1D or 2D (list of lists) list. Each element is a dictionary with the following format
                  {'data': Data to be plotted, 'text': Label, 'axis': X-Axis to be shown, 'image': True/False}
        names: optional Names to be given to each plot.
        share: Boolean pair Indicates if plots have to be plotted on the same sub-plot
    returns:
        None
    """
    if not isinstance(arr_list[0], list):
        arr_list = [arr_list]
        if names != None:
            names = [names]

    len_h = len(arr_list)
    len_v = len(arr_list[0])
    if share[0] == True:
        len_h = 1
    if share[1] == True:
        len_v = 1
    fig, axes = plt.subplots(len_h, len_v)
    if len_h == 1:
        axes = np.expand_dims(axes, 0)
    if len_v == 1:
        axes = np.expand_dims(axes, 1)
    for i, lst in enumerate(arr_list):
        for j, arr_desc in enumerate(arr_list[i]):
            arr = np.atleast_2d(arr_desc['data'])
            axis_data = arr_desc.get('axis', None)
            text = arr_desc.get('text', None)
            idx_i = i
            idx_j = j
            if share[0] == True:
                idx_i = 0
            if share[1] == True:
                idx_j = 0
            is_image = arr_desc.get('image', False)
            if is_image:
                axes[idx_i, idx_j].imshow(arr, cmap='gray')
            else:
                num_plots = arr.shape[0]
                for p in range(num_plots):
                    text_p = text
                    if (text is not None) and (type(text) is not str):
                        text_p = text[p]
                    if axis_data is not None:
                        axes[idx_i, idx_j].plot(
                            axis_data, arr[p, :], label=text_p)
                    else:
                        axes[idx_i, idx_j].plot(arr[p, :], label=text_p)
                axes[idx_i, idx_j].legend()
            if names != None:
                axes[idx_i, idx_j].set_title(names[idx_i][idx_j])

    plt.show()
