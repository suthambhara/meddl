import torch
from utils.tensorutils import move_tensors_to_device
from utils.DebugUtils import WriteRec
from utils.DebugUtils import WriteTiff
import argparse
import pathlib
from library.model import Model
from Denoise.denoise_model import DenoiseModel
from Denoise.dataset import NaturalImagesTif
from Denoise.data_transform import DataTransforms
import numpy
# import os
# os.environ["CUDA_VISIBLE_DEVICES"] = ""


default_chkpt = pathlib.Path(
    "Denoise/models/latest_Denoise_Gaussian_1channel_1_snr.pt")

default_save_Trace_path = pathlib.Path(
    "Denoise/models/latest_Denoise_Gaussian_1channel_1_snr_cpu.pth")
default_save_Trace_path = None


def create_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=pathlib.Path, default=pathlib.Path("/media/suthambhara/Backup Plus/test"),
                        help='Path to the tiff data')
    parser.add_argument('--checkpoint', type=pathlib.Path, default=default_chkpt,
                        help='Model checkpoint for inference')
    parser.add_argument('--export-folder', type=pathlib.Path, default=pathlib.Path("/media/suthambhara/Backup Plus/test/export"),
                        help='model export folder')
    parser.add_argument('-t', '--trace', type=pathlib.Path,  default=default_save_Trace_path,
                        help="trace and save model path")
    parser.add_argument('--device', type=str, default='cuda',
                        help='Which device to run inference on on. cuda or cpu are the choices')

    return parser


def get_lr(epoch: int) -> float:
    n = epoch // 10000
    return 1.0 / (n+1)


if __name__ == '__main__':
    args = create_arg_parser().parse_args()

    test_data = NaturalImagesTif(
        str(args.data), transform=DataTransforms("validation", 0.1, noise_range=[0.05, 0.05]))

    data_loader = torch.utils.data.DataLoader(
        test_data, batch_size=1, shuffle=False, pin_memory=True)

    if args.checkpoint != None:
        model = Model.load_model(
            DenoiseModel, str(args.checkpoint), training=False, model_params_override={'device': args.device})

        if args.trace is None:
            with torch.no_grad():
                for i, im in enumerate(data_loader):
                    move_tensors_to_device(im,  args.device)
                    result = model(im)
                    op = result['output'].cpu().detach().numpy()
                    f_name = str(args.export_folder / ('result_'+str(i)))
                    #WriteRec(op, f_name)
                    op = numpy.squeeze(op)
                    WriteTiff(op, f_name)

                    f_name_data = str(args.export_folder / ('data_'+str(i)))
                    #WriteRec(im['data'].cpu().detach().numpy(), f_name_data)
                    WriteTiff(im['data'].cpu().detach().numpy(), f_name_data)

                    f_name_target = str(
                        args.export_folder / ('target_'+str(i)))
                    WriteRec(im['target'].cpu().detach().numpy(),
                             f_name_target)
                    WriteTiff(im['target'].cpu().detach().numpy(),
                             f_name_target)
                    
        else:
            iteraa = iter(data_loader)
            data_dictionary = next(iteraa)
            move_tensors_to_device(data_dictionary,  args.device)
            traced_model = model.trace_model(
                (data_dictionary['data']), None, str(args.trace))
