from library.model import Model
from library.networks.unet import Unet3D
import torch
import torch.nn as nn
from typing import Tuple
from collections import OrderedDict


class Unet3DLung(Unet3D):
    def __init__(self):
        super(Unet3DLung, self).__init__()

    def get_double_conv(self, in_channels, out_channels, level, basename=None):
        conv1 = self.get_conv_layer(
            in_channels, out_channels, level, basename+"_conv_1")

        conv2 = self.get_conv_layer(
            out_channels, out_channels,  level, basename+"_conv_2")
        batch_norm = nn.BatchNorm3d(out_channels)

        lyr = nn.Sequential(OrderedDict({
            'conv_1': conv1,
            'activ_1': nn.LeakyReLU(0.1, inplace=True),
            'conv_2': conv2,
            'activ_2': nn.LeakyReLU(0.1, inplace=True),
            'batch_norm': batch_norm
        }))
        return lyr

    # Not required. we combine logits version of BCE
    # def get_final_conv_block(self):
    #     final_layer = nn.Sequential(OrderedDict({
    #         'conv_1': self.get_conv_layer(
    #             self.units, self.units // 2, None, 'unet_finalconv1'),
    #         'activ_1': nn.LeakyReLU(0.1, inplace=True),
    #         'conv_2':  self.get_conv_layer(
    #             self.units // 2, self.out_channels, None, "unet_finalconv2"),
    #         'activ_2': nn.Sigmoid()
    #     }))
    #     return final_layer


class LungNoduleModel(Model):

    def __init__(self, model_config: dict):
        super(LungNoduleModel, self).__init__(model_config)
        self.segmenter = Unet3DLung()
        self.pad_size = 8

    def forward(self, input_dict: dict):
        data = input_dict['data']
        padded_data = torch.nn.functional.pad(
            data, (self.pad_size, self.pad_size, self.pad_size,
                   self.pad_size, self.pad_size, self.pad_size))

        segmented_data = self.segmenter(padded_data)
        segmented_data = segmented_data[..., self.pad_size:-
                                        self.pad_size, self.pad_size:-
                                        self.pad_size, self.pad_size:-self.pad_size]
        return {'output': segmented_data}

    def post_model_execution(self, input_dict: dict) -> dict:
        """
        Overriden from Model
        """
        return input_dict
