import torch
import numpy
from LungCTNodule.data_transform import DataTransforms
from LungCTNodule.dataset import LNDBDataset
from LungCTNodule.lung_model import LungNoduleModel
from library.model import Model
import pathlib
import argparse

default_chkpt = None
default_chkpt = pathlib.Path(
    "LungCTNodule/models/latest_noduleseg_2.pt")


def create_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=pathlib.Path, default=pathlib.Path("/home/suthambhara/drive/Devel/challenges/LN/Lndb/h5_out/training"),
                        help='Path to the h5 data')
    parser.add_argument('--validation-data', type=pathlib.Path, default=pathlib.Path("/home/suthambhara/drive/Devel/challenges/LN/Lndb/h5_out/validation"),
                        help='Path to the h5 data')
    parser.add_argument('--epochs', default=50000, type=int,
                        help='Number of epochs of training')
    parser.add_argument('--batch-size', type=int, default=4,
                        help='Size of the batches used during training')
    parser.add_argument('--id', type=str, default="noduleseg_with_zoomaug",
                        help='id of the run')
    parser.add_argument('--checkpoint', type=pathlib.Path, default=default_chkpt,
                        help='Model checkpoint to restart the training')
    parser.add_argument('--export-folder', type=pathlib.Path, default=pathlib.Path('LungCTNodule/models'),
                        help='model export folder')
    parser.add_argument('--logdir', type=pathlib.Path, default=pathlib.Path('LungCTNodule/logs'),
                        help='Logging directory for Tensorboard')
    return parser


def get_lr(epoch: int) -> float:

    step_fac = 0.0025
    return step_fac


if __name__ == '__main__':
    args = create_arg_parser().parse_args()

    data = LNDBDataset(str(args.data), transform=DataTransforms("training"))

    val_data = LNDBDataset(
        str(args.validation_data), transform=DataTransforms("validation"))

    loader = torch.utils.data.DataLoader(
        data, batch_size=args.batch_size, num_workers=2, shuffle=True, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        val_data, batch_size=1, shuffle=False, pin_memory=True)

    if args.checkpoint != None:
        model = Model.load_model(
            LungNoduleModel, str(args.checkpoint), training=True, load_optimizer=True)
        training_config = {'epochs': args.epochs, 'id': args.id,
                           'export_folder': str(args.export_folder),
                           'clip_gradient_value': 10.0}
        model.train_model(loader, val_loader, training_config=training_config)
    else:
        model = LungNoduleModel({'device': 'cuda', 'in_channels': 1})

        model.compile_model('Adam', 'BCELossWithLogits')

        training_config = {'epochs': args.epochs, 'id': args.id,
                           'export_folder': str(args.export_folder),
                           'clip_gradient_value': 10.0}

        logging_config = {'path': str(args.logdir),
                          'batch_interval': 10, 'epoch_interval': 1}

        scheduling_config = {'type': 'LambdaLR', 'params': {'lambda': get_lr}}

        model.train_model(loader, val_loader, training_config, scheduler_config=scheduling_config, validation_config={'epoch_interval': 1, 'batch_interval': 10},
                          logging_config=logging_config)
