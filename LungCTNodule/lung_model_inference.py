import torch
from utils.tensorutils import move_tensors_to_device
from utils.DebugUtils import WriteRec
import argparse
import pathlib
from library.model import Model
from LungCTNodule.data_transform import DataTransforms
from LungCTNodule.dataset import LNDBDataset
from LungCTNodule.lung_model import LungNoduleModel
import numpy
# import os
# os.environ["CUDA_VISIBLE_DEVICES"] = ""


default_chkpt = pathlib.Path(
    "LungCTNodule/models/latest_noduleseg_2.pt")

default_save_Trace_path = pathlib.Path(
    "LungCTNodule/models/latest_noduleseg_1.pth")
default_save_Trace_path = None


def create_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=pathlib.Path, default=pathlib.Path("/home/suthambhara/drive/Devel/challenges/LN/Lndb/h5_out/validation"),
                        help='Path to the h5 data')
    parser.add_argument('--checkpoint', type=pathlib.Path, default=default_chkpt,
                        help='Model checkpoint for inference')
    parser.add_argument('--export-folder', type=pathlib.Path, default=pathlib.Path('/home/suthambhara/drive/Devel/challenges/LN/Lndb/h5_out/results'),
                        help='model export folder')
    parser.add_argument('-t', '--trace', type=pathlib.Path,  default=default_save_Trace_path,
                        help="trace and save model path")
    parser.add_argument('--device', type=str, default='cuda',
                        help='Which device to run inference on on. cuda or cpu are the choices')

    return parser


def get_lr(epoch: int) -> float:
    n = epoch // 10000
    return 1.0 / (n+1)


if __name__ == '__main__':
    args = create_arg_parser().parse_args()

    val_data = LNDBDataset(
        str(args.data), transform=DataTransforms("validation"))

    val_loader = torch.utils.data.DataLoader(
        val_data, batch_size=1, shuffle=False, pin_memory=True)

    if args.checkpoint != None:
        model = Model.load_model(
            LungNoduleModel, str(args.checkpoint), training=False, model_params_override={'device': args.device})

        if args.trace is None:
            with torch.no_grad():
                for i, im in enumerate(val_loader):
                    move_tensors_to_device(im,  args.device)
                    result = model(im)
                    op = result['output'].cpu().detach().numpy()
                    f_name = str(args.export_folder / ('result_'+str(i)))
                    WriteRec(op, f_name)

                    f_name_data = str(args.export_folder / ('data_'+str(i)))
                    WriteRec(im['data'].cpu().detach().numpy(), f_name_data)

                    f_name_target = str(
                        args.export_folder / ('target_'+str(i)))
                    WriteRec(im['target'].cpu().detach().numpy(),
                             f_name_target)
        # else:
        #     iteraa = iter(data_loader)
        #     data_dictionary = next(iteraa)
        #     move_tensors_to_device(data_dictionary,  args.device)
        #     traced_model = model.trace_model(
        #         (data_dictionary['data']), None, str(args.trace))
