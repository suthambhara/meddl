import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from collections import OrderedDict
from typing import Tuple


class Unet(nn.Module):
    """
        Base class for all Unets in meddl.
    """

    def __init__(self, in_channels: int = 1, out_channels: int = 1, units: int = 32, depth: int = 3, kernelsize: int = 3, **kwargs):
        """
            Args:
                in_channels(int) : Number of input channels
                out_channels(int): Number of output channels
                units(int): Number of units at the highest level in the Unet
                depth(int) : Number of levels. This is equal to one more than the downsampling modules
                kernelsize(int): Convolution kernel sizes

        """
        super(Unet, self).__init__()
        self.units = units
        self.depth = depth
        self.kernel_size = kernelsize
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.downsamplepath = nn.ModuleList()
        self.upsamplepath = nn.ModuleList()

        self.channel_dim = 1
        inp_channels = self.in_channels

        numunits = self.units
        for d in range(self.depth - 1):
            basename = "unet_downsample_"+str(d)
            downsample_block = self.get_downsample_block(
                inp_channels, numunits, d, basename)
            self.downsamplepath.append(downsample_block)
            inp_channels = numunits
            if d < self.depth - 2:
                numunits *= 2

        self.lower_layer = self.get_double_conv(
            numunits, numunits, None, "unet_lowest")

        numunits = self.units
        op_channels = numunits
        for d in range(self.depth - 1):
            basename = "unet_upsample_"+str(d)
            self.upsamplepath.append(
                self.get_upsample_block(2*numunits, op_channels, d, basename))
            if d > 0:
                op_channels *= 2
            numunits *= 2

        self.last_block = self.get_final_conv_block()

    def forward(self, inputs: torch.tensor) -> torch.Tensor:
        op = inputs
        # Store output after each level in downsample path
        output_at_each_level = []
        # input shapes would be needed for interpolation later
        input_shapes_at_each_level = []

        for blks in self.downsamplepath:

            input_shapes_at_each_level.append(
                self.get_shape_vector(op))

            op = blks['double_conv'](op)
            output_at_each_level.append(op)
            op = blks['downsample'](op)

        op = self.lower_layer(op)

        for i in np.arange(self.depth-2, -1, -1):
            # Threshold output from downsampling path

            ip_from_down_sample = output_at_each_level[i]

            op = self.upsamplepath[i]['upsample'](
                op, input_shapes_at_each_level[i])
            # one of the inputs to higher level is the thresholded values
            op = torch.cat(
                [ip_from_down_sample, op], self.channel_dim)

            op = self.upsamplepath[i]['double_conv'](op)

        op = self.last_block(op)

        return op

    def get_shape_vector(self, input: torch.Tensor) -> Tuple[int, ...]:
        raise NotImplementedError(
            "get_shape_vector needs to be implemented by derived class")

    def get_conv_layer(self, num_units: int, level: int, layer_name: str, activ):
        raise NotImplementedError(
            "get_conv_layer needs to be implemented by derived class")

    def get_downsample_layer(self, level, layer_name):
        raise NotImplementedError(
            "get_downsample_layer needs to be implemented")

    def get_upsample_layer(self, layer_name):
        raise NotImplementedError(
            "get_upsample_layer needs to be implemented by derived class")

    def get_double_conv(self, in_channels, out_channels, level, basename=None):
        conv1 = self.get_conv_layer(
            in_channels, out_channels, level, basename+"_conv_1")

        conv2 = self.get_conv_layer(
            out_channels, out_channels,  level, basename+"_conv_2")

        lyr = nn.Sequential(OrderedDict({
            'conv_1': conv1,
            'activ_1': nn.LeakyReLU(0.1, inplace=True),
            'conv_2': conv2,
            'activ_2': nn.LeakyReLU(0.1, inplace=True)
        }))
        return lyr

    def get_downsample_block(self, in_channels, out_channels, level, basename=None):
        if basename == None:
            basename = "unet_downsample_"+str(level)

        dsample = self.get_downsample_layer(level)

        double_conv = self.get_double_conv(
            in_channels, out_channels, level, basename)

        return nn.ModuleDict({'downsample': dsample, 'double_conv': double_conv})

    def get_final_conv_block(self):

        final_layer = nn.Sequential(OrderedDict({
            'conv_1': self.get_conv_layer(
                self.units, self.units // 2, None, 'unet_finalconv1'),
            'activ_1': nn.LeakyReLU(0.1, inplace=True),
            'conv_2':  self.get_conv_layer(
                self.units // 2, self.out_channels, None, "unet_finalconv2"),
            'activ_2': nn.LeakyReLU(0.1, inplace=True)
        }))

        return final_layer

    def get_upsample_block(self, in_channels, out_channels,  level, basename=None):
        if basename == None:
            basename = "unet_upsample_"+str(level)

        usample = self.get_upsample_layer()

        double_conv = self.get_double_conv(
            in_channels, out_channels, level, basename)

        return nn.ModuleDict({'upsample': usample, 'double_conv': double_conv})


class UpSample(nn.Module):
    def __init__(self):
        super(UpSample, self).__init__()

    def forward(self, x, output_size):
        return torch.nn.functional.interpolate(x, output_size, align_corners=False, mode='bilinear')


class UpSample3D(nn.Module):
    def __init__(self):
        super(UpSample3D, self).__init__()

    def forward(self, x, output_size):
        return torch.nn.functional.interpolate(x, output_size, mode='nearest')


class Unet2D(Unet):
    def __init__(self, in_channels=1, out_channels=1, units=32, depth=3,
                 kernelsize=3,  **kwargs):
        super(Unet2D, self).__init__(in_channels, out_channels, units, depth,
                                     kernelsize, ** kwargs)

    def get_conv_layer(self, in_channels, out_channels, level=None, layer_name=None):
        conv = nn.Conv2d(in_channels, out_channels, self.kernel_size, 1,
                         (self.kernel_size-1)//2, bias=True, padding_mode='zeros')
        return conv

    def get_downsample_layer(self, level=None, layer_name=None):
        dsample = nn.MaxPool2d(2)
        return dsample

    def get_upsample_layer(self, layer_name=None):
        return UpSample()

    def get_shape_vector(self, data: torch.Tensor):
        input_shp = data.shape

        return (input_shp[-2], input_shp[-1])


class Unet3D(Unet):
    def __init__(self, in_channels=1, out_channels=1, units=32, depth=3,
                 kernelsize=3,  **kwargs):
        super(Unet3D, self).__init__(in_channels, out_channels, units, depth,
                                     kernelsize, ** kwargs)

    def get_conv_layer(self, in_channels, out_channels, level=None, layer_name=None):
        conv = nn.Conv3d(in_channels, out_channels, self.kernel_size, 1,
                         (self.kernel_size-1)//2, bias=True, padding_mode='zeros')
        return conv

    def get_downsample_layer(self, level=None, layer_name=None):
        dsample = nn.MaxPool3d(2)
        return dsample

    def get_upsample_layer(self, layer_name=None):
        return UpSample3D()

    def get_shape_vector(self, data: torch.Tensor):
        input_shp = data.shape

        return (input_shp[-3], input_shp[-2], input_shp[-1])
