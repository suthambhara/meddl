import h5py
import numpy as np
import mhdutils
import argparse
import pathlib
import pandas as pd


def create_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=pathlib.Path,
                        default=pathlib.Path(
                            '/home/suthambhara/drive/Devel/challenges/LN/Lndb'),
                        help='Path to the metal data')
    parser.add_argument('--outdir', type=pathlib.Path,
                        default=pathlib.Path(
                            '/home/suthambhara/drive/Devel/challenges/LN/Lndb/h5_out'),
                        help='Path to the out dir')
    return parser


if __name__ == '__main__':
    parser = create_arg_parser().parse_args()
    data_path = parser.data
    out_dir = parser.outdir
    out_dir.mkdir(exist_ok=True)
    data_desc = pd.read_csv(str(data_path / 'trainNodules.csv'))
    for idx, rows in data_desc.iterrows():
        lndbid = int(rows['LNDbID'])
        radid = int(rows['RadID'])
        findingid = int(rows['FindingID'])
        loc_nodule = rows[['x', 'y', 'z']].to_numpy()
        nodule = int(rows['Nodule'])
        volume = rows['Volume']
        texture = rows['Text']
        out_file_name = pathlib.Path(
            '_'.join([str(lndbid), str(radid), str(findingid)]))
        out_file_name = out_dir / out_file_name.with_suffix('.h5')
        if not out_file_name.exists():
            [scan, spacing, origin, transfmat] = mhdutils.readMhd(
                str(data_path / 'data/LNDb-{:04}.mhd'.format(lndbid)))
            print(spacing, origin, transfmat)
            # Read segmentation mask
            [mask, spacing, origin, transfmat] = mhdutils.readMhd(
                str(data_path / 'masks/LNDb-{:04}_rad{}.mhd'.format(lndbid, radid)))

            transfmat_toimg, transfmat_toworld = mhdutils.getImgWorldTransfMats(
                spacing, transfmat)
            ctr = mhdutils.convertToImgCoord(
                loc_nodule, origin, transfmat_toimg)

            # Display nodule scan/mask slice
            from matplotlib import pyplot as plt
            fig, axs = plt.subplots(1, 2)
            axs[0].imshow(scan[int(ctr[2])])
            axs[1].imshow(mask[int(ctr[2])])
            plt.show()

            scan_cube = mhdutils.extractCube(scan, spacing, ctr)
            mask_cube = mhdutils.extractCube(mask, spacing, ctr)

            h_file = h5py.File(str(out_file_name), 'w')
            g = h_file.create_group('AllData')
            g.create_dataset('scan', data=scan_cube)
            g.create_dataset('mask', data=mask_cube)
            g.create_dataset('Nodule', data=nodule)
            g.create_dataset('Volume', data=volume)
            g.create_dataset('Text', data=texture)
            h_file.close()

    pass

    pass
