import torch
import numpy as np
from torchvision import transforms
from typing import Tuple


class DataTransforms:
    def __init__(self, phase: str, normalization: float, noise_range: Tuple[float, float] = [0.01, 0.01]):
        self.normalization = normalization
        self.phase = phase
        self.noise_range = noise_range
        if phase == 'training':
            self.transform = transforms.Compose(
                [self.normalize, self.rotate, self.add_noise])
        else:
            self.transform = transforms.Compose(
                [self.normalize, self.add_noise])

    def add_noise(self, input_dict):

        data = input_dict['data']
        gaussian_noise_levels = self.noise_range
        std_dev = np.random.uniform(
            gaussian_noise_levels[0], gaussian_noise_levels[1])
        gaussian_part = np.random.normal(
            0.0, std_dev, (data.shape[0], data.shape[1], data.shape[2])).astype('float32')

        data += gaussian_part

        data = data / std_dev
        data = np.clip(data, 0, None)
        target = input_dict['target']
        target /= std_dev
        data *= self.normalization
        target *= self.normalization

        input_dict['target'] = target
        input_dict['data'] = data
        input_dict['normalization'] = self.normalization / std_dev
        return input_dict

    def rotate(self, input_dict):
        data = input_dict['data']
        target = input_dict['target']

        transformation = np.random.randint(0, 4)
        # augment
        if transformation > 0:
            data = np.rot90(data, transformation, axes=(-2, -1)).copy()
            target = np.rot90(target, transformation, axes=(-2, -1)).copy()

        input_dict['data'] = data
        input_dict['target'] = target
        return input_dict

    def normalize(self, input_dict):
        data = input_dict['data']
        target = input_dict['target']

        min_val = np.percentile(data, 0.001)
        max_val = np.percentile(data, 99.999)

        if (max_val - min_val) > 0.00001:
            data = (data - min_val) / (max_val - min_val)
            data = np.clip(data, 0.0, 1.0)

            target = (target - min_val) / (max_val - min_val)
            target = np.clip(target, 0.0, 1.0)
        else:
            data.fill(0.0)
            target.fill(0.0)
            print(f'Warning : Blank image used min={min_val}, max={max_val}')

        input_dict['data'] = data
        input_dict['target'] = target
        return input_dict

    def __call__(self, input_dict: dict):
        transformed_dict = self.transform(input_dict)
        return transformed_dict
