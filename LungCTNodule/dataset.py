import torch
from torch.utils.data.dataset import Dataset
import pathlib
import os
import os.path
import numpy as np
import h5py
from typing import Any, Tuple
# D:\Devel\data\Denoise\Images\train


class LNDBDataset(Dataset):

    def __init__(self, images_path: str, **kwargs: dict):
        assert os.path.isdir(
            images_path) == True, "A directory containing images in h5 format is expected"
        file_path = pathlib.Path(images_path)

        if file_path.is_dir():
            self.file_list = list(file_path.glob('**/*.h5'))
        else:
            self.file_list = [file_path]
            assert(file_path.suffix == '.h5')
        print('Reading files')
        self.h5_files = []
        for file in self.file_list:
            try:
                h_file = h5py.File(str(file), 'r')['AllData']
                self.h5_files.append(h_file)
            except:
                print("Could not read %s" % (str(file)))
        print('Reading done')

        self.args = kwargs

        self.transform = self.args.get('transform', None)
        if self.transform is not None:
            assert callable(self.transform), "Transform should be a callable"

    def read_data(self, h5_root: Any) -> Tuple[np.array, np.array]:
        scan_data = h5_root['scan'][:]
        mask_data = h5_root['mask'][:]
        scan_data = np.expand_dims(scan_data, 0)
        mask_data = np.expand_dims(mask_data, 0)
        # Don't distinguish between findings
        mask_data[mask_data > 0] = 1
        return (scan_data.astype('float32'), mask_data.astype('float32'))

    def __getitem__(self, idx):
        (scan_data, mask_data) = self.read_data(self.h5_files[idx])

        sample = {'data': scan_data, 'target': mask_data}

        if self.transform is not None:
            sample = self.transform(sample)
        return sample

    def __len__(self):
        return len(self.file_list)
