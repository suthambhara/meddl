import torch
from torch.utils.data.dataset import Dataset
import pathlib
import os
import os.path
import numpy as np
import tifffile
import PIL
# D:\Devel\data\Denoise\Images\train


class NaturalImagesTif(Dataset):

    def __init__(self, images_path: str, num_channels: int = 1, **kwargs: dict):
        assert os.path.isdir(
            images_path) == True, "A directory containing images in Tif format is expected"
        self.file_list = list(pathlib.Path(images_path).glob('**/*.tif'))
        self.args = kwargs
        self.patch_len = self.args.get("patch_len", 0)
        self.num_channels = num_channels
        self.transform = self.args.get('transform', None)
        if self.transform is not None:
            assert callable(self.transform), "Transform should be a callable"

    def __getitem__(self, idx):
        data = tifffile.imread(
            str(self.file_list[idx])).astype('float32')
        xlen = data.shape[0]
        ylen = data.shape[1]
        supported_path_len = self.patch_len
        if supported_path_len > 0:
            assert((xlen >= supported_path_len)
                   and (ylen >= supported_path_len))
            x_start = np.random.randint(0, xlen-supported_path_len)
            y_start = np.random.randint(0, ylen-supported_path_len)
            data = data[x_start:x_start+supported_path_len,
                        y_start:y_start+supported_path_len, ...].copy()

        data = np.expand_dims(data, 0)
        target = data.copy()

        data = np.tile(data, (self.num_channels, 1, 1))

        sample = {'data': data, 'target': target}

        if self.transform is not None:
            sample = self.transform(sample)
        return sample

    def __len__(self):
        return len(self.file_list)


class NaturalImagesJpg(Dataset):

    def __init__(self, images_path: str, num_channels: int = 1, **kwargs: dict):
        assert os.path.isdir(
            images_path) == True, "A directory containing images in Tif format is expected"
        self.file_list = list(pathlib.Path(images_path).glob('**/*.jpg'))
        self.args = kwargs
        self.patch_len = self.args.get("patch_len", 0)
        self.num_channels = num_channels
        self.transform = self.args.get('transform', None)
        if self.transform is not None:
            assert callable(self.transform), "Transform should be a callable"

    def __getitem__(self, idx):
        data = np.asarray(PIL.Image.open(str(self.file_list[idx])))
        data = data.astype('float32')

        xlen = data.shape[0]
        ylen = data.shape[1]
        supported_path_len = self.patch_len
        if supported_path_len > 0:
            assert((xlen >= supported_path_len)
                   and (ylen >= supported_path_len))
            x_start = np.random.randint(0, xlen-supported_path_len)
            y_start = np.random.randint(0, ylen-supported_path_len)
            data = data[x_start:x_start+supported_path_len,
                        y_start:y_start+supported_path_len, ...].copy()

        data = np.transpose(data, (2, 0, 1))
        target = data.copy()

        data = np.tile(data, (self.num_channels, 1, 1))

        sample = {'data': data, 'target': target}

        if self.transform is not None:
            sample = self.transform(sample)
        return sample

    def __len__(self):
        return len(self.file_list)
