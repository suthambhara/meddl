import torch
from library.networks.unet import Unet2D


if __name__ == "__main__":
    with torch.no_grad():
        un = Unet2D()
        inp = torch.zeros([1, 1, 128, 128])
        op = un(inp)
