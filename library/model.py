import torch
import torch.nn as nn
from typing import Union, Tuple
from library.losses import MedDLLoss, L1_MSSSIM, BCELossWithLogits
from utils.tensorutils import move_tensors_to_device
import sys
from torch.optim.lr_scheduler import StepLR, LambdaLR
from torch.utils.tensorboard import SummaryWriter
import pathlib


class Model(nn.Module):

    def __init__(self, init_params: dict):
        super(Model, self).__init__()
        assert 'device' in init_params, "Device should be specified cpu/cuda are the choices"
        self._meddl_device = init_params['device']
        self._meddl_initialized = True
        self._meddl_compiled = False
        self._meddl_moved_to_device = False
        self._meddl_optimizer_config = {}
        self._meddl_loss_config = {}
        self._meddl_optimizer_obj = None
        self._meddl_loss_obj = None
        self.model_config = init_params
        self._meddl_training_config = {}
        self._meddl_lr_scheduler = None
        self._meddl_lr_scheduler_config = {}
        self._meddl_validation_config = {}
        self._meddl_logging_config = {}
        self._meddl_summary_writer = None

    def check_initialized(self):
        assert self._meddl_initialized == True, "Model not initialized. You need to call  super(<YourModel>, self).__init__() as the first line in your _init__()"

    def check_compiled(self):
        assert self._meddl_compiled == True, "Model not compiled. Call compile_model if you are in training mode"

    def get_model_optimizer(self, optimizer:  Union[str, dict, None]) -> Tuple[dict, torch.optim.Optimizer]:
        supported_opts = {'Adam': {'lr': 0.0001}}

        if optimizer is None:
            raise NotImplementedError(
                "if optimizer is None You must override get_model_optimizer and return ({'type':name, 'params':parameters}, optimizerobject")
        # self.parameters()
        if type(optimizer) == str:
            optimizer = {'type': optimizer}

        opt_name = optimizer['type']

        assert opt_name in supported_opts, "Optimizer not supported"
        opt_parameters = optimizer.get('params', supported_opts[opt_name])

        if(opt_name == 'Adam'):
            return optimizer, torch.optim.Adam(
                self.parameters(), lr=opt_parameters['lr'])

        raise ValueError("Optimizer not implemented")

    def get_model_loss(self, loss:  Union[str, dict, None]) -> Tuple[dict, torch.nn.Module]:
        supported_losses = {'MSE': None, 'L1': None,
                            'MSSIM': None, 'L1_MSSSIM': None, 'BCELossWithLogits': None}
        if loss is None:
            raise NotImplementedError(
                "You must override get_model_loss if loss is None")

        if type(loss) == str:
            loss = {'type': loss}

        loss_name = loss['type']

        assert loss_name in supported_losses, "Loss type not supported"

        loss_params = loss.get('params', supported_losses[loss_name])

        if loss_name == 'L1_MSSSIM':
            return loss, L1_MSSSIM(loss_params)
        if loss_name == 'BCELossWithLogits':
            return loss, BCELossWithLogits(loss_params)

        return loss, MedDLLoss(loss_name, loss_params)

    def get_optimizer_scheduler(self, scheduler: Union[str, dict]):
        supported_scheds = {'StepLR': {
            'step_size': 30, 'gamma': 0.1}, 'LambdaLR': {'lambda': lambda epoch:  1.0}}
        if type(scheduler) == str:
            scheduler = {'type': scheduler}

        sched_name = scheduler['type']
        assert sched_name in supported_scheds, "scheduler type not supported"
        sched_params = scheduler.get('params', supported_scheds[sched_name])

        if sched_name == 'StepLR':
            lr_scheduler = StepLR(
                self._meddl_optimizer_obj, sched_params['step_size'], sched_params['gamma'], last_epoch=scheduler.get('last_epoch', -1))
            return scheduler, lr_scheduler

        if sched_name == 'LambdaLR':
            lr_scheduler = LambdaLR(
                self._meddl_optimizer_obj, lr_lambda=sched_params['lambda'], last_epoch=scheduler.get('last_epoch', -1))
            return scheduler, lr_scheduler

        raise ValueError("Scheduler not implemented")

    def compile_model(self, optimizer: Union[str, dict, None], loss: Union[str, dict, None]):
        """
        Moves the model to device, Sets the optimizer and loss function for training
        Arguments:
            optimizer[dict]
             'type' : Type name of optimizer
            'params' : Dictionary containing parameters for the optimizer

            loss[dict]
            'type' : Type name of optimizer
            'params' : Dictionary containing parameters for the optimizer

        """
        self.check_initialized()

        if self._meddl_compiled:
            print("Recompiling model with new arguments")

        self._move_to_device()

        self._meddl_optimizer_config, self._meddl_optimizer_obj = self.get_model_optimizer(
            optimizer)

        self._meddl_loss_config, self._meddl_loss_obj = self.get_model_loss(
            loss)

        self._meddl_compiled = True

    @ staticmethod
    def load_model(model_class, model_path: str, training: bool, load_optimizer: bool = True, model_params_override: dict = None):
        saved_file_dict = torch.load(model_path)
        model_config = saved_file_dict['model']['model_config']
        if model_params_override is not None:
            model_config.update(model_params_override)
        model = model_class(model_config)
        if training:
            model._meddl_training_config = saved_file_dict['training_config']
            model._meddl_logging_config = saved_file_dict['logging_config']
            model._meddl_validation_config = saved_file_dict['validation_config']
            model._meddl_training_config['start_epoch_num'] = model._meddl_training_config['epochs_completed']
            model.compile_model(
                saved_file_dict['optimizer']['optimizer_config'], saved_file_dict['loss_config'])
            if load_optimizer:
                model._meddl_optimizer_obj.load_state_dict(
                    saved_file_dict['optimizer']['state'])
                model._meddl_lr_scheduler_config = saved_file_dict['scheduler_config']

        model.load_state_dict(saved_file_dict['model']['state'])
        if training:
            model.train()
        else:
            # For inference move the model to device.model_params_override can be used to change the device from what was used during training
            model._move_to_device()
            model.eval()

        return model

    def get_model_save_name_training(self) -> str:
        return self._meddl_training_config.get('id', 'experiment_0')

    def get_model_save_name_validation(self) -> str:
        return 'val_best_'+self._meddl_traisning_config.get('id', 'experiment_0')

    def save_model(self, name: str):
        torch.save({'scheduler_config': self._meddl_lr_scheduler_config,
                    'logging_config': self._meddl_logging_config,
                    'training_config': self._meddl_training_config,
                    'validation_config': self._meddl_validation_config,
                    'loss_config': self._meddl_loss_config,
                    'optimizer': {'optimizer_config': self._meddl_optimizer_config,
                                  'state': self._meddl_optimizer_obj.state_dict()},
                    'model': {'model_config': self.model_config, 'state': self.state_dict()}
                    }, name
                   )

    def post_model_execution(self, input_dict: dict) -> dict:
        """
        Override this routine if user defined operations on the resulting output after model execution is required
        """
        return input_dict

    def get_tracer(self, trace_params: None) -> nn.Module:
        """
        Override this for tracing. This function should return a trace module
        """
        raise NotImplementedError(
            "get_tracer needs to be overriden by deriuved model")

    def trace_model(self, in_tensors: Tuple[torch.Tensor, ...], trace_model_params: dict, filename: str = None) -> nn.Module:
        """
        Traces a torch script model. Calling this would require that the model has an implemented get_tracer routine.
        Arguments:

            in_tensors  Tuple of tensors in same order expected by the tracer model returned by get_tracer

            trace_model_params[dict] parameters that will be passed to get_tracer

            filename  Filename for saving the script. can be None

        Returns:
            traced callable script
        """
        trace_obj = self.get_tracer(trace_model_params)
        with torch.no_grad():
            with torch.jit.optimized_execution(should_optimize=True):
                traced_script = torch.jit.trace(
                    trace_obj, in_tensors)
                if filename is not None:
                    traced_script.save(filename)
                return traced_script

    def validate_model(self, data_loader_validation: torch.utils.data.DataLoader, validation_config: dict = None) -> float:
        self.eval()
        with torch.no_grad():
            val_loss = 0
            total_samples = 0
            for inputs in data_loader_validation:
                assert type(
                    inputs) is dict, "Data loader should always output a dictionary with tensors"

                self.__move_tensors_to_device(inputs)
                inputs['result'] = self.__call__(inputs)
                inputs = self.post_model_execution(inputs)

                loss: torch.Tensor = self._meddl_loss_obj(inputs)

                batch_size = self._getbatch_size(inputs['result'])

                val_loss += batch_size * loss.item()
                total_samples += batch_size

        return val_loss / total_samples

    def write_epoch_log(self):
        if self._meddl_summary_writer:
            epochs_completed = self._meddl_training_config.get(
                'epochs_completed', 1) - 1
            epoch_loss = self._meddl_training_config['epoch_loss']
            self._meddl_summary_writer.add_scalar(
                'loss/epoch_loss', epoch_loss, epochs_completed)

    def write_batch_log(self):
        if self._meddl_summary_writer:
            samples_seen = self._meddl_training_config['num_samples_seen']

            epoch_loss = self._meddl_training_config['batch_loss']
            self._meddl_summary_writer.add_scalar(
                'loss/batch_loss', epoch_loss, samples_seen)

    def write_validation_log(self):
        if self._meddl_summary_writer:
            samples_seen = self._meddl_training_config['num_samples_seen']

            val_loss = self._meddl_validation_config['val_loss']
            self._meddl_summary_writer.add_scalar(
                'loss/val_loss', val_loss, samples_seen)

    def train_model(self, data_loader: torch.utils.data.DataLoader, data_loader_validation: torch.utils.data.DataLoader = None,
                    training_config: dict = None, validation_config: dict = None, scheduler_config: Union[dict, str] = None, logging_config: dict = None):
        """
        Trains the derived model

        Arguments:
        training_config[dict] :
            'epochs': Number of epochs to run,
            'id': training id. could be a string
            'export_folder' : Folder to export trained model. would be current folder if not passed
            'clip_gradient_value' : clip gradients during backpropagation to this value. by default this is disabled

        scheduler_config[dict] :
            'type' : Type name of scheduler
            'params' : Dictionary containing parameters for the scheduler

        validation_config[dict] :
            'epoch_interval': interval of epoch units for validation,
            'batch_interval': interval of batch units for validation, pass -1 to disable

        logging_config[dict]:
            'path'  : Directory to write log
            'epoch_interval': interval of epoch units for logging
            'batch_interval':  interval of batch units for logging, pass -1 to disable

        """
        self.check_compiled()

        self._set_training_config(training_config)

        self._set_validation_config(validation_config)

        self._set_scheduler_config(scheduler_config)

        self._set_logging_config(logging_config)

        start_epoch_num = self._meddl_training_config.get('start_epoch_num', 0)
        num_epochs = self._meddl_training_config['epochs']

        for epoch in range(start_epoch_num, start_epoch_num+num_epochs):

            print('Epoch {}/{}'.format(epoch, start_epoch_num+num_epochs - 1))
            print('-' * 10)
            self.train()
            epoch_loss, _ = self._train_one_epoch(
                data_loader, data_loader_validation)

            self._meddl_lr_scheduler.step()
            self._update_epoch_training_information(epoch_loss, epoch)

            self._test_and_validate_after_epoch(epoch, data_loader_validation)

            self._test_and_write_epoch_log(epoch)

            # Save model after every epoch
            self._save_model(
                'latest_'+self.get_model_save_name_training()+'.pt')

        if self._meddl_summary_writer != None:
            self._meddl_summary_writer.close()

    def __move_tensors_to_device(self, inputs: dict):
        move_tensors_to_device(inputs, self._meddl_device)

    def _move_to_device(self):
        if self._meddl_moved_to_device == False:
            self.to(self._meddl_device)
            self._meddl_moved_to_device = True

    def _getbatch_size(self, input_dict: dict):
        """
            Get batch size of first tensor in dict
        """
        batch_size = 1
        for elem in input_dict:
            if type(input_dict[elem]) is torch.Tensor:
                batch_size = input_dict[elem].shape[0]
                break
        return batch_size

    def _train_one_epoch(self, data_loader: torch.utils.data.DataLoader, validation_data_loader) -> Tuple[float, int]:
        num_elems_epoch = 0
        epoch_loss = 0
        clipvalue = self._meddl_training_config.get('clip_gradient_value', 0.0)
        for batch_num, inputs in enumerate(data_loader):
            assert type(
                inputs) is dict, "Data loader should always output a dictionary with tensors"
            self.__move_tensors_to_device(inputs)
            inputs['result'] = self.__call__(inputs)
            loss_from_model = inputs['result'].get('loss', None)
            if loss_from_model is not None:
                loss = loss_from_model
            else:
                inputs = self.post_model_execution(inputs)
                loss: torch.Tensor = self._meddl_loss_obj(inputs)

            self._meddl_optimizer_obj.zero_grad()

            loss.backward()
            if clipvalue > 0.0:
                torch.nn.utils.clip_grad_norm_(self.parameters(), clipvalue)

            self._meddl_optimizer_obj.step()

            batch_size = self._getbatch_size(inputs['result'])

            epoch_loss += batch_size * loss.item()
            num_elems_epoch += batch_size
            self._meddl_training_config['batch_loss'] = loss.item()
            self._meddl_training_config['num_samples_seen'] = self._meddl_training_config['num_samples_seen'] + batch_size
            self._test_and_write_batch_log(
                batch_num, epoch_loss / num_elems_epoch)
            self._test_and_validate_after_batch(
                batch_num, validation_data_loader)

        epoch_loss = epoch_loss / num_elems_epoch

        print(f'Average epoch loss={epoch_loss}')
        return epoch_loss, num_elems_epoch

    def _set_validation_config(self, validation_config: dict):

        if validation_config is not None:
            self._meddl_validation_config.update(validation_config)

        # Default is to validate every epoch
        self._meddl_validation_config['epoch_interval'] = self._meddl_validation_config.get(
            'epoch_interval', 1)
        self._meddl_validation_config['batch_interval'] = self._meddl_validation_config.get(
            'batch_interval', -1)
        self._meddl_validation_config['epoch_intervals_seen'] = self._meddl_validation_config.get(
            'epoch_intervals_seen', 0)

        self._meddl_validation_config['best_val_loss'] = self._meddl_validation_config.get(
            'best_val_loss', sys.float_info.max)

    def _set_training_config(self, training_config: dict):
        if (training_config is None) and (len(self._meddl_training_config) == 0):
            raise ValueError(
                "training_config can be None only if training from a saved model")
        if training_config is not None:
            self._meddl_training_config.update(training_config)

        self._meddl_training_config['best_epoch_loss'] = self._meddl_training_config.get(
            'best_epoch_loss', sys.float_info.max)

        self._meddl_training_config['num_samples_seen'] = self._meddl_training_config.get(
            'num_samples_seen', 0)

    def _set_scheduler_config(self, scheduler_config: dict):

        if scheduler_config is not None:
            self._meddl_lr_scheduler_config.update(scheduler_config)

        self._meddl_lr_scheduler_config['type'] = self._meddl_lr_scheduler_config.get(
            'type', 'LambdaLR')

        self._meddl_lr_scheduler_config, self._meddl_lr_scheduler = self.get_optimizer_scheduler(
            self._meddl_lr_scheduler_config)

    def _set_logging_config(self, logging_config: dict):

        if logging_config is not None:
            self._meddl_logging_config.update(logging_config)

        self._meddl_logging_config['epoch_intervals_seen'] = self._meddl_logging_config.get(
            'epoch_intervals_seen', 0)
        self._meddl_logging_config['epoch_interval'] = self._meddl_logging_config.get(
            'epoch_interval', 1)
        self._meddl_logging_config['batch_interval'] = self._meddl_logging_config.get(
            'batch_interval', -1)

        logging_path = self._meddl_logging_config.get('path', None)
        if logging_path is not None:
            self._meddl_summary_writer = SummaryWriter(
                logging_path+'/'+self._meddl_training_config.get('id', 'experiment_0'))

    def _test_and_write_batch_log(self, batch_num, running_loss):
        batch_interval = self._meddl_logging_config['batch_interval']
        if batch_interval > 0:
            if batch_num % batch_interval == 0:
                self.write_batch_log()
                print(f"Running Epoch loss = {running_loss}")

    def _test_and_write_epoch_log(self, epoch):
        if self._meddl_logging_config['epoch_interval'] > 0:
            if self._meddl_logging_config['epoch_interval'] * self._meddl_logging_config['epoch_intervals_seen'] <= epoch:
                self._meddl_logging_config['epoch_intervals_seen'] = self._meddl_logging_config['epoch_intervals_seen'] + 1
                self.write_epoch_log()
            if self._meddl_summary_writer != None:
                self._meddl_summary_writer.flush()

    def _test_and_validate_after_epoch(self, epoch: int, data_loader_validation: torch.utils.data.DataLoader):
        if self._meddl_validation_config['epoch_interval'] > 0:
            if self._meddl_validation_config['epoch_interval'] * self._meddl_validation_config['epoch_intervals_seen'] <= epoch:
                self._meddl_validation_config['epoch_intervals_seen'] = self._meddl_validation_config['epoch_intervals_seen'] + 1
                self._validate_point(data_loader_validation)

    def _test_and_validate_after_batch(self, batch_num: int,  data_loader_validation: torch.utils.data.DataLoader):
        batch_interval = self._meddl_validation_config['batch_interval']
        if (batch_interval > 0) and (batch_num % batch_interval == 0):
            self._validate_point(data_loader_validation)
            # reset training status
            self.train()

    def _validate_point(self, data_loader_validation: torch.utils.data.DataLoader):
        if data_loader_validation is not None:
            print(f'Validating')
            print('-' * 10)
            val_loss = self.validate_model(
                data_loader_validation, self._meddl_validation_config)
            self._meddl_validation_config['val_loss'] = val_loss
            print(f'Average Validation loss={val_loss}')
            print('-' * 10)
            if self._meddl_validation_config['best_val_loss'] > val_loss:
                self._meddl_validation_config['best_val_loss'] = val_loss
                self._save_model(
                    'best_val_' + self.get_model_save_name_training()+'.pt')
            self.write_validation_log()

    def _save_model(self, name: str):
        export_folder = self._meddl_training_config.get('export_folder', './')
        export_path = pathlib.Path(export_folder) / name
        self.save_model(str(export_path))

    def _update_epoch_training_information(self, epoch_loss: float, epoch: int):
        if self._meddl_training_config['best_epoch_loss'] > epoch_loss:
            self._meddl_training_config['best_epoch_loss'] = epoch_loss
            self._save_model(
                'best_'+self.get_model_save_name_training()+'.pt')

        self._meddl_training_config['epochs_completed'] = epoch + 1
        self._meddl_training_config['epoch_loss'] = epoch_loss
        self._meddl_lr_scheduler_config['last_epoch'] = epoch
